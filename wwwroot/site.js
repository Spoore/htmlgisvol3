const uri = 'api/todo';
let todos = null;
function getCount(data) {
    const el = $('#counter');
    let name = 'packeges to deliver';
    if (data) {
        if (data > 1) {
            name = 'packages';
        }
        el.text(data + ' ' + name);
    } else {
        el.html('No ' + name);
    }
}

$(document).ready(function () {
    getData();
});

function getData() {
    $.ajax({
        type: 'GET',
        url: uri,
        success: function (data) {
            $('#todos').empty();
            getCount(data.length);
            $.each(data, function (key, item) {
                const checked = item.isComplete ? 'checked' : '';

                $('<tr><td><input disabled="true" type="checkbox" ' + checked + '></td>' +
                    '<td>' + item.name + '</td>' +
                    '<td><button onclick="editItem(' + item.id + ')">Edit</button></td>' +
                    '<td><button onclick="deleteItem(' + item.id + ')">Delete</button></td>' +
                    '</tr>').appendTo($('#todos'));
            });

            todos = data;
        }
    });
}

function addItem() {
    const item = {
        'name': $('#add-name').val(),
        'Volume': $('#add-size').val(),
        'Location': $('#adres').val(),
        'ClientSurname': $('#nazwisko-dobiorcy').val(),
        'ClientName': $('#imie-odbiorcy').val(),    
        'isComplete': false
    };

    $.ajax({
        type: 'POST',
        accepts: 'application/json',
        url: uri,
        contentType: 'application/json',
        data: JSON.stringify(item),
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Błędne dane');
        },
        success: function (result) {
            var value = $('#add-size').val();
            console.log(value);
            getData();
            $('#add-name').val('');
            $('#add-size').val('');
            $('#adres').val('');
            $('#nazwisko-dobiorcy').val('');
            $('#imie-odbiorcy').val('');
            defaultmap();        
        }
    });
}

function deleteItem(id) {
    $.ajax({
        url: uri + '/' + id,
        type: 'DELETE',
        success: function (result) {
            getData();
        }
    });
}

function editItem(id) {
    $.each(todos, function (key, item) {
        if (item.id === id) {
            $('#edit-name').val(item.name);
            $('#edit-size').val(item.volume);
            $('#edit-imie-odbiorcy').val(item.clientName);
            $('#edit-nazwisko-dobiorcy').val(item.clientSurname);
            $('#edit-adres').val(item.location);
            $('#edit-id').val(item.id);
            $('#edit-isComplete')[0].checked = item.isComplete;
        }
    });
    $('#spoiler').css({ 'display': 'block' });
    simpleReverseGeocoding(document.getElementById('edit-adres').value);
}

$('.my-form').on('submit', function () {
    const item = {
        'name': $('#edit-name').val(),
        'Volume': $('#edit-size').val(),
        'Location': $('#edit-adres').val(),
        'ClientSurname': $('#edit-nazwisko-dobiorcy').val(),
        'ClientName': $('#edit-imie-odbiorcy').val(),  
        'isComplete': $('#edit-isComplete').is(':checked'),
        'id': $('#edit-id').val()
    };

    $.ajax({
        url: uri + '/' + $('#edit-id').val(),
        type: 'PUT',
        accepts: 'application/json',
        contentType: 'application/json',
        data: JSON.stringify(item),
        success: function (result) {
            getData();
            defaultmap();  
        }
    });

    closeInput();
    return false;
});

function closeInput() {
    $('#spoiler').css({ 'display': 'none' });
}


var map = new ol.Map({
    layers: [
      new ol.layer.Tile({
        source: new ol.source.OSM()
      })
    ],
    target: 'map',
    view: new ol.View({
  center: ol.proj.fromLonLat([21.0067265, 52.2319237]),
      zoom: 6
    })
  });
  function simpleReverseGeocoding(text) {
    fetch('https://nominatim.openstreetmap.org/search?q=' + text + '&format=json&polygon_geojson=1').then(function(response) {
      return response.json();
    }).then(function(jsondata) {
  var lon = parseFloat(JSON.parse(JSON.stringify(jsondata[0].lon)));
  var lat = parseFloat(JSON.parse(JSON.stringify(jsondata[0].lat)));
  map.getView().setCenter(ol.proj.fromLonLat([lon, lat]));
  map.getView().setZoom(18);
    })
  }

  function defaultmap() {
    map.getView().setCenter(ol.proj.fromLonLat([21.0067265, 52.2319237]));
    map.getView().setZoom(6);
  }
  

  document.getElementById('geocoding').addEventListener('click', function(e) {
    if (document.getElementById('adres').value) {
      simpleReverseGeocoding(document.getElementById('adres').value);
    }
  });

  document.getElementById('geocoding2').addEventListener('click', function(e) {
      simpleReverseGeocoding(document.getElementById('edit-adres').value);
  });