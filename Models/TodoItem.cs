using Microsoft.EntityFrameworkCore;
namespace TodoApi.Models
{
    public class TodoItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsComplete { get; set; }
        public int Volume{get; set;}
        public string Location{get;set;}
        public string ClientSurname{get;set;}
        public string ClientName{get;set;}
    }
}